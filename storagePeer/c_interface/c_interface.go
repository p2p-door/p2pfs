package main

import (
	"C"
	"log"
	"storagePeer/src/peer"
)

//export UploadFileRSC
func UploadFileRSC(ringIP string, fname string, ringsz uint64, fcontent []byte) {

	if err := peer.UploadFileRSC(ringIP, fname, ringsz, fcontent); err != nil {
		log.Println("Error uploading file (RSC)!", err)
	}
}

//export DownloadFileRSC
func DownloadFileRSC(ringIP string, fname string, ringsz uint64, fcontent []byte) int {

	emptySpace, err := peer.DownloadFileRSC(ringIP, fname, ringsz, fcontent)
	if err != nil {
		log.Println("Error downloading file (RSC)!", err)
	}

	return emptySpace
}

//export DeleteFileRSC
func DeleteFileRSC(ringIP string, fname string, ringsz uint64) {

	if err := peer.DeleteFileRSC(ringIP, fname, ringsz); err != nil {
		log.Println("Error deleting file (RSC!", err)
	}
}

func main() {
}

//package main
//
//import (
//	"flag"
//	"fmt"
//	"storagePeer/src/peer"
//	"strconv"
//)
//
//const (
//	stringMargin = "ASDJTFGYAKUSJHKFJ;AJGALDKSFNASJKHBFASUKJHFBSDHKNAHMBSFASFASJB,FSJAH,BFASJKFSA:"
//)
//
//func getMessageCount(ip string, ringSize uint64) (uint64, error) {
//	for sizeMultiplier := 1; ; sizeMultiplier++ {
//		buf := make([]byte, sizeMultiplier*256)
//		cnt, err := peer.DownloadFileRSC(ip, "Count", ringSize, buf)
//		//fmt.Println(cnt, buf)
//		if err != nil {
//			return 0, nil
//		}
//		if cnt > 0 {
//			countStr := string(buf[:256*sizeMultiplier-cnt])[len(stringMargin):]
//			countStr = countStr[:len(countStr)-1]
//			//fmt.Println(countStr)
//			i, err := strconv.ParseUint(countStr, 10, 64)
//			if err != nil {
//				return 0, err
//			}
//			//fmt.Println(i)
//			return i, nil
//		}
//	}
//}
//
//func updateMessageCount(newCount uint64, ip string, ringSize uint64) error {
//	_ = peer.DeleteFileRSC(ip, "Count", ringSize)
//	return peer.UploadFileRSC(ip, "Count", ringSize, []byte(stringMargin+strconv.FormatUint(newCount, 10)))
//}
//
//func writeMessage(message, ip string, ringSize uint64) error {
//	currentCount, err := getMessageCount(ip, ringSize)
//	if err != nil {
//		return err
//	}
//
//	err = updateMessageCount(currentCount+1, ip, ringSize)
//	if err != nil {
//		return err
//	}
//
//	err = peer.UploadFileRSC(ip, strconv.FormatUint(currentCount, 10), ringSize, []byte(message))
//	if err != nil {
//		return err
//	}
//
//	return nil
//}
//
//func displayMessages(ip string, ringSize uint64) error {
//	currentCount, err := getMessageCount(ip, ringSize)
//	if err != nil {
//		return err
//	}
//	if currentCount == 0 {
//		fmt.Println("No messages")
//		return nil
//	}
//
//	for fileNumber := uint64(0); fileNumber < currentCount; fileNumber++ {
//		for sizeMultiplier := 1; ; sizeMultiplier++ {
//			buf := make([]byte, sizeMultiplier*256)
//			cnt, err := peer.DownloadFileRSC(ip, strconv.FormatUint(fileNumber, 10), ringSize, buf)
//			if err != nil {
//				return err
//			}
//			if cnt > 0 {
//				fmt.Println(string(buf[:256*sizeMultiplier-cnt]))
//				break
//			}
//		}
//	}
//
//	return nil
//}
//
//func main() {
//	ringsz := flag.Uint64("num", 0, "Number of nodes in the network")
//	ringIP := flag.String("entry", "", "Ip of some existing node")
//	actionCode := flag.Uint64("mode", 0, "Number of action")
//	message := flag.String("message", "", "Message to send to network")
//	name := flag.String("name", "Anonymous", "Sender name")
//	flag.Parse()
//
//	if *ringIP == "" {
//		panic("ip flag not set")
//	}
//	if *ringsz == 0 {
//		panic("num flag not set")
//	}
//	if *actionCode == 1 {
//		if *message == "" {
//			panic("blank message")
//		}
//		*message = *name + ": " + *message
//		err := writeMessage(*message, *ringIP, *ringsz)
//		if err != nil {
//			panic(err)
//		}
//	} else if *actionCode == 2 {
//		err := displayMessages(*ringIP, *ringsz)
//		if err != nil {
//			panic(err)
//		}
//	} else {
//		//_ = peer.DeleteFileRSC(*ringIP, "Count", *ringsz)
//		panic("No such action available")
//	}
//}

package peer

import (
	"fmt"
)

// Routine to fix corrupt files
func (p *Peer) fixRoutine() error {
	const MAX_FILESIZE = 4096 * 1600 / 8
	filecont := make([]byte, MAX_FILESIZE)
	selfIP, ringsz := p.ring.RingInfo()

	for {
		newFile := <-p.ring.NewFilesChannel
		empty, err := DownloadFileRSC(selfIP, newFile, ringsz, filecont)
		if err != nil {
			fmt.Println(err)
			return err
		}

		err = UploadFileRSC(selfIP, newFile, ringsz, filecont[:MAX_FILESIZE-empty])
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
}
